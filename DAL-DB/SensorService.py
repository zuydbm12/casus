__author__ = 'David'

from DBConnection import DBConnection
class SensorService:

    # make a database connection
    def __init__(self):
        self.db = DBConnection()

    # creating a new sensor
    def create(self, type, placement):
        self.db.create('Sensor', ['Type', 'Placement'], [type, placement])

    def selectById(self, id):
        return self.db.read('Sensor', {'SensorId': id})

    # Might return multiple records
    def selectByType(self, type):
        return self.db.read('Sensor', {'Type': type})

    def selectByPlacement(self, placement):
        return self.db.read('Sensor', {'Placement': placement})

    # delete sensor based on type and placement for example delete de temperature sensor in living room east
    def delete(self, type, placement):
        self.db.delete('Sensor', {'Type': type, 'Placement': placement})

    def deleteByType(self, type):
        self.db.delete('Sensor', {'Type': type})

    def deleteByPlacement(self, placement):
        self.db.delete('Sensor', {'Placement': placement})