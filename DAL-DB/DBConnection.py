# Database connection + generic get and insert

import sqlite3


class DBConnection:

    # setting up the connection and making a cursor which is used to execute sql statements
    def __init__(self):
        self.con = sqlite3.connect('data.db')
        self.curs = self.con.cursor()

    # sensorId is created by us, not automatically
    # run this method ONLY once
    def createTables(self):
        self.curs.execute('''CREATE TABLE Sensor (SensorId INTEGER primary key, Type nvarchar(25) NOT NULL, Placement nvarchar(50) NOT NULL)''')
        self.curs.execute('''CREATE TABLE MotionDetection (MDMId INTEGER primary key, SensorId int, Detected_on timestamp NOT NULL, foreign key(SensorId) references Sensor(SensorId))''')
        # Appearance is in percentages, so for example 21 for oxigen
        self.curs.execute('''CREATE TABLE AirComposition (ACId INTEGER primary key, SensorId int, Gas nvarchar(40) NOT NULL, Appearance double NOT NULL, Detected_on timestamp NOT NULL, foreign key(SensorId) references Sensor(SensorId))''')
        self.curs.execute('''CREATE TABLE Airpressure (APId INTEGER primary key, SensorId int, Pressure int NOT NULL, Detected_on timestamp NOT NULL, foreign key(SensorId) references Sensor(SensorId))''')
        self.curs.execute('''CREATE TABLE Temperature (TId INTEGER primary key, SensorId int, Temperature double NOT NULL, Detected_on timestamp NOT NULL, foreign key(SensorId) references Sensor(SensorId))''')

        # commit the changes to the database to make them persistent
        self.con.commit()
        

    # For example: insert(AirComposition, [SensorId, Gas, Appearance, Detected_on], [1234567890, 'oxigen', 21.05, str(datetime.datetime.now())])
    def create(self, tableName, columns, values):
        columnstr = ''

        if columns:
            columnstr += '('
            for column in columns:
                columnstr += column + ','
            # remove last comma and add the closing brace
            columnstr = columnstr[:-1]
            columnstr += ')'

        valuestr = ''

        if values:
            valuestr = 'VALUES('
            for value in values:
                valuestr += "'" + str(value) + "'" + ","
        
            # remove last comma and add the closing brace
            valuestr = valuestr[:-1]
            valuestr += ')'

        fullStmt = "INSERT INTO "+tableName+" "+columnstr+" "+valuestr
        self.curs.execute(fullStmt)
        self.con.commit()



    # generic get method that can be used to get every and any record from every and any table
    def read(self, tableName, conditions={}, columnNames='*'):
        condstr = ''
        
        if conditions:
            condstr += ' WHERE '
            for key in conditions:
                if key == 'Detected_on':
                    condstr += key + ' like ' + "'" + conditions[key] + "'" + ' AND '
                else:
                    condstr += key + ' = ' + "'" + conditions[key] + "'" + ' AND '
        # removing the last AND
        condstr = condstr[:-5]

        fullStmt = "SELECT "+columnNames+" FROM "+tableName+condstr
        
        self.curs.execute(fullStmt)
        return self.curs.fetchall()

    # generic delete method to delete a record from a specified table
    def delete(self, tableName, conditions):
        condstr = ""

        if conditions:
            condstr += " WHERE "
            for key in conditions:
                condstr += key + ' = ' + "'" + conditions[key] + "'" + ' AND '
        condstr = condstr[:-5]

        fullStmt = "SELECT FROM " + tableName + condstr

        self.curs.execute(fullStmt)
        self.con.commit()

