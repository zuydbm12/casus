__author__ = 'David'

from DBConnection import DBConnection
from datetime import datetime, timedelta

class AirCompositionService:

    def __init__(self):
        self.db = DBConnection()

    def createAirComposition(self, sensorId, gas, appearance, detected_on):
        self.db.create('AirComposition', ['SensorId', 'Gas', 'Appearance', 'Detected_on'], [sensorId, gas, appearance, detected_on])

    def selectBySensorId(self, sensorId):
        return self.db.read('AirComposition', {'SensorId': sensorId})

    def selectByGas(self, gas):
        return self.db.read('AirComposition', {'Gas': gas})

    def selectByDate(self, date):
        return self.db.read('AirComposition', {'Detected_on': date})

    # select per date then combine
    # input is 2 dates that only consist of yyyy-MM-dd, so no time
    def selectByDateInterval(self, startdate, enddate):
        records = []
        date = startdate
        while date < enddate:
            # extend the records list with the new day
            records.extend(self.db.read('AirComposition', {'Detected_on': date}))
            date = date + timedelta(days=1)
        return records

    def deleteBySensorId(self, sensorId):
        self.db.delete('AirComposition', {'SensorId': sensorId})

    def deleteByDate(self, date):
        self.db.delete('AirComposition', {'Detected_on': date})

    def deleteByDateInterval(self, startdate, enddate):
        date = startdate
        while date < enddate:
            # extend the records list with the new day
            self.db.delete('AirComposition', {'Detected_on': date})
            date = date + timedelta(days=1)