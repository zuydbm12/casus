from enum import Enum
__author__ = 'Barry Lendertz'

class Evaluation(Enum):
    Below = 1
    Between = 2
    Above = 3
    Danger = 4

# The class that keeps track of the data.
class Blueprint():
    # The static arrays that contain the most suitable value for each stat.
    # Each value in the array represents 5 sec.
    Temperature = []
    Humidity = []
    CO1 = []
    CO2 = []
    AirPressure = []

    # Percentage of how much the measured value may deviate from the blueprint.
    deviation = 10

    # The TLV-values (Threshold Limit Value)
    Co1Mac = 25
    Co2Mac = 5000

    # Initialize
    def __init__(self, _temp, _humidity, _CO1, _CO2, _airPressure):
        self.Temperature = _temp
        self.Humidity = _humidity
        self.CO1 = _CO1
        self.CO2 = _CO2
        self.AirPressure = _airPressure

    # Temperature check
    def EvaluateTemp(self, _temp):
        # Check if the temperature is lower than usual
        if (self.Temperature * (1 - (self.deviation / 100)) > _temp):
            return Evaluation.Below

        # Check if the temperature is higher than usual
        if (self.Temperature * (1 + (self.deviation / 100)) < _temp):
            return Evaluation.Above

        return Evaluation.Between

    # Humidity check
    def EvaluateHumidity(self, _humidity):
        # Check if the humidity is lower than usual
        if( self.Humidity * (1 - (self.deviation / 100)) > _humidity):
            return Evaluation.Below

        # Check if the humidity is higher than usual
        if (self.Humidity * (1 - (self.deviation / 100)) > _humidity):
            return Evaluation.Above

        return Evaluation.Between

    # CO1 check
    def EvaluateCO1(self, _CO1):
        # Check if the CO1 exceeds the TLV-value
        if (_CO1 > self.Co1Mac):
            return Evaluation.Danger

        # Check if the CO1 is higher than the usual CO1
        if (self.CO1 * (1 + (self.deviation / 100)) < _CO1):
            return Evaluation.Above

        return False

    # CO2 check
    def EvaluateCO2(self, _CO2):
        # Check if the CO1 exceeds the TLV-value
        if (_CO2 > self.Co2Mac):
            return Evaluation.Danger

        # Check if the CO1 is higher than the usual CO1
        if (self.CO2 * (1 + (self.deviation / 100)) < _CO2):
            return Evaluation.Above

        return False

    # Air pressure check
    def EvaluateAirPressure(self, _airPressure):
        # Check if the air pressure is lower than usual
        if (self.AirPressure * (1 - (self.deviation / 100)) < _airPressure):
            return Evaluation.Below

        # Check if the air pressure is higher than usual
        if (self.AirPressure * (1 + (self.deviation / 100)) > _airPressure):
            return Evaluation.Above
        return False







