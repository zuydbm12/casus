from django.db import models
from django.forms import ModelForm

# Create your models here.
class Sensor(models.Model):
    Type = models.CharField(max_length=25)
    Placement = models.CharField(max_length=50)

class AirComposition(models.Model):
    GAS_CHOICES = (
        ('LV', 'Luchtvochtigheid'),
        ('CO', 'KoolstofMonoxide'),
        ('CO2', 'KoolstofDioxide'),
        ('CH4', 'Methaan'),
        )
    SensorId = models.ForeignKey('Sensor')
    Gas = models.CharField(max_length=40,choices=GAS_CHOICES)
    Appearence = models.DecimalField(max_digits=3, decimal_places=1)
    Detected_on = models.DateTimeField(auto_now_add=True)

class Temperature(models.Model):
    SensorId = models.ForeignKey('Sensor')
    Value = models.DecimalField(max_digits=3, decimal_places=1)
    Detected_on = models.DateTimeField(auto_now_add=True)

class AirPressure(models.Model):
    SensorId = models.ForeignKey('Sensor')
    Pressure = models.IntegerField()
    Detected_on = models.DateTimeField(auto_now_add=True)

class MotionDetection(models.Model):
    SensorId = models.ForeignKey('Sensor')
    Detected_on = models.DateTimeField(auto_now_add=True)

class AirCompositionForm(ModelForm):
    class Meta:
        model = AirComposition
        fields = ['Gas','Appearence']

class TemperatureForm(ModelForm):
    class Meta:
        model = Temperature
        fields = ['Value']

class AirPressureForm(ModelForm):
    class Meta:
        model = AirPressure
        fields = ['Pressure']

class SensorForm(ModelForm):
    class Meta:
        model = Sensor
        fields = ['Type','Placement']

class MotionDetectionForm(ModelForm):
    class Meta:
        model = MotionDetection
        exclude =['Detected_on','SensorId']

### OLD MODELS
##class Temperature(models.Model):
##    Temperatuur = models.DecimalField(max_digits=3, decimal_places=1)
##    Sigma = models.DecimalField(max_digits=2, decimal_places=1)
##    Datum = models.DateField(auto_now_add=True)
##    Tijd = models.TimeField(auto_now_add=True)
##    GrenswaardeOnder = models.DecimalField(max_digits=3, decimal_places=1)
##    GrenswaardeBoven = models.DecimalField(max_digits=3, decimal_places=1)
##
##class TemperatureForm(ModelForm):
##    class Meta:
##        model = Temperature
##        fields = ['Temperatuur','Sigma']
##
##class LV(models.Model):
##    LuchtVochtigheid = models.DecimalField(max_digits=3, decimal_places=1)
##    Sigma = models.DecimalField(max_digits=2, decimal_places=1)
##    Datum = models.DateField(auto_now_add=True)
##    Tijd = models.TimeField(auto_now_add=True)
##    GrenswaardeOnder = models.DecimalField(max_digits=3, decimal_places=1)
##    GrenswaardeBoven = models.DecimalField(max_digits=3, decimal_places=1)
##
##class LVForm(ModelForm):
##    class Meta:
##        model = LV
##        fields = ['LuchtVochtigheid','Sigma']
##
##class CO(models.Model):
##    KoolstofMonoxide = models.DecimalField(max_digits=3, decimal_places=1)
##    Sigma = models.DecimalField(max_digits=2, decimal_places=1)
##    Datum = models.DateField(auto_now_add=True)
##    Tijd = models.TimeField(auto_now_add=True)
##    GrenswaardeBovenCO = models.DecimalField(max_digits=3, decimal_places=1)
##
##class COForm(ModelForm):
##    class Meta:
##        model = CO
##        fields = ['KoolstofMonoxide']
##
##class CO2(models.Model):
##    KoolstofDioxide = models.DecimalField(max_digits=3, decimal_places=1)
##    Sigma = models.DecimalField(max_digits=2, decimal_places=1)
##    Datum = models.DateField(auto_now_add=True)
##    Tijd = models.TimeField(auto_now_add=True)
##    GrenswaardeBovenCO2 = models.DecimalField(max_digits=3, decimal_places=1)
##
##class CO2Form(ModelForm):
##    class Meta:
##        model = CO2
##        fields = ['KoolstofDioxide']
##
##class CH4(models.Model):
##    Methaan = models.DecimalField(max_digits=3, decimal_places=1)
##    Sigma = models.DecimalField(max_digits=2, decimal_places=1)
##    Datum = models.DateField(auto_now_add=True)
##    Tijd = models.TimeField(auto_now_add=True)
##    GrenswaardeBovenCH4 = models.DecimalField(max_digits=3, decimal_places=1)
##
##class CH4Form(ModelForm):
##    class Meta:
##        model = CH4
##        fields = ['Methaan']
