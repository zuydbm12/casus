from django.shortcuts import render, render_to_response
from django.http import HttpResponse, HttpResponseRedirect
from django.urls import reverse
from chartit import DataPool, Chart

import time
#import RPi.GPIO as GPIO

from .models import *

#Globals
currentT = 15
currentLV = 50
newT = 15
newLV = 50

#RPi settings:
#GPIO.setmode(GPIO.BCM)
# Pin 17: blue LED
#BlueLED = 17
#GPIO.setup(BlueLED, GPIO.OUT)
# Pin 18: red LED
#RedLED = 18
#GPIO.setup(RedLED, GPIO.OUT)
#RGB:
#RGBBlueLED = 2
#RGBGreenLED = 3
#RGBRedLED = 4
#GPIO.setup(RGBBlueLED, GPIO.OUT)
#GPIO.setup(RGBGreenLED, GPIO.OUT)
#GPIO.setup(RGBRedLED, GPIO.OUT)

# Create your views here.
def index(request):
    template_name = 'Sis/index.html'
    
    global currentT
    global currentLV
    global newT
    global newLV
    
    context = {
        'currentT': currentT,
        'currentLV': currentLV,
        'newT': newT,
        'newLV': newLV,
        }
    
    if request.method == 'GET':
        return render(request, 'Sis/index.html', context)
    else:
        if(request.POST.get('changeT')):
            newT = int(request.POST.get('newT'))
            if (currentT < newT):             
                while(currentT < newT):
                    currentT += 1
                    #GPIO.output(RedLED, True)
                    time.sleep(1)
                #GPIO.output(RedLED, False)
            else:
                while(currentT > newT):
                    currentT -= 1
                    #GPIO.output(BlueLED, True)
                    time.sleep(1)
                #GPIO.output(BlueLED, False)
            return HttpResponseRedirect(reverse('index'))
        elif(request.POST.get('changeLV')):
            newLV = int(request.POST.get('newLV'))
            if (currentLV < newLV):
                while(currentLV < newLV):
                    currentLV += 1
                    #GPIO.output(RGBGreenLED, True)
                    time.sleep(1)
                #GPIO.output(RGBGreenLED, False)
            else:
                while(currentLV > newLV):
                    currentLV -= 1
                    time.sleep(1)
            return HttpResponseRedirect(reverse('index'))
    return render(request, 'Sis/index.html')            

def addSensor(request):
    model = Sensor
    template_name = 'SIS/addSensor.html'

    if request.method == 'GET':
        form = SensorForm()
    else:
        form = SensorForm(request.POST)

        if form.is_valid():
            Type = form.cleaned_data['Type']
            Placement = form.cleaned_data['Placement']
            sensor = Sensor.objects.create(Type=Type,
                                           Placement=Placement)
            return HttpResponseRedirect(reverse('addSensor'))
    return render(request, 'Sis/addSensor.html', {'form' : form})

def addTemperature(request):
    model = Temperature
    template_name = 'SIS/addTemperature.html'

    if request.method == 'GET':
        form = TemperatureForm()
    else:
        form = TemperatureForm(request.POST)

        if form.is_valid():
            SensorId = Sensor.objects.get(Type='Temperature')
            Value = form.cleaned_data['Value']
            temperature = Temperature.objects.create(SensorId=SensorId,
                                                     Value=Value)
            return HttpResponseRedirect(reverse('addTemperature'))
    return render(request, 'Sis/addTemperature.html', {'form' : form})

def addAirComposition(request):
    model = AirComposition
    template_name = 'Sis/addAirComposition.html'

    if request.method == 'GET':
        form = AirCompositionForm()
    else:
        form = AirCompositionForm(request.POST)

        if form.is_valid():
            SensorId = Sensor.objects.get(Type='AirComposition')
            Gas = form.cleaned_data['Gas']
            Appearence = form.cleaned_data['Appearence']
            aircomposition = AirComposition.objects.create(SensorId=SensorId,
                                                           Gas=Gas,
                                                           Appearence=Appearence)                                                           
            return HttpResponseRedirect(reverse('addAirComposition'))
    return render(request, 'Sis/addAirComposition.html', {'form' : form})

def addAirPressure(request):
    model = AirPressure
    template_name = 'Sis/addAirPressure.html'

    if request.method == 'GET':
        form = AirPressureForm()
    else:
        form = AirPressureForm(request.POST)

        if form.is_valid():
            SensorId = Sensor.objects.get(Type='AirPressure')
            Pressure = form.cleaned_data['Pressure']
            airpressure = AirPressure.objects.create(SensorId=SensorId,
                                                     Pressure=Pressure)
            return HttpResponseRedirect(reverse('addAirPressure'))
    return render(request, 'Sis/addAirPressure.html', {'form' : form})

def addMotionDetection(request):
    model = MotionDetection
    template_name = 'Sis/addMotionDetection.html'

    if request.method == 'GET':
        form = MotionDetectionForm()
    else:
##        if(request.POST.get('setPlacement')):
##            placement = str(request.POST.get('placement'))
##            
        form = MotionDetectionForm(request.POST)

        if form.is_valid():
            SensorId = Sensor.objects.get(Type='MotionDetection',Placement='Front door')
            motiondetection = MotionDetection.objects.create(SensorId=SensorId)
            return HttpResponseRedirect(reverse('addMotionDetection'))
    return render(request, 'Sis/addMotionDetection.html', {'form' : form})


def plotTemperature(request):
    if(request.method == 'POST'):
        if(request.POST.get('Submit')):             
             date = str(request.POST.get('date'))
             source = Temperature.objects.filter(Detected_on__contains=date)
             temperaturedata = \
                DataPool(
                    series=
                    [{'options': {
                        'source': source},
                        'terms': [
                            'Value',
                            'Detected_on']}
                    ])

                #Step 2: Create the Chart object
             cht = Chart(
                    datasource = temperaturedata,
                    series_options =
                        [{'options':{
                            'type': 'line',
                            'stacking': False},
                        'terms':{
                            'Detected_on': [
                                'Value']
                        }}],
                    chart_options =
                        {'title': {
                            'text': 'Temperatuur over een bepaalde periode'},
                        'xAxis': {
                            'title': {
                                'text': 'Tijd'}},
                        'yAxis': {
                            'title': {
                                'text': 'Temperatuur in graden Celsius'}}})
                #Step 3: Send the chart object to the template.
             return render(request, 'Sis/plotTemperature.html', {'temperaturechart': cht})
    else:
        source = Temperature.objects.all()
    #Step 1: Create a DataPool with the data we want to retrieve.
    temperaturedata = \
        DataPool(
            series=
            [{'options': {
                'source': source},
                'terms': [
                    'Value',
                    'Detected_on']}
            ])

        #Step 2: Create the Chart object
    cht = Chart(
            datasource = temperaturedata,
            series_options =
                [{'options':{
                    'type': 'line',
                    'stacking': False},
                'terms':{
                    'Detected_on': [
                        'Value']
                }}],
            chart_options =
                {'title': {
                    'text': 'Temperatuur over een bepaalde periode'},
                'xAxis': {
                    'title': {
                        'text': 'Tijd'}},
                'yAxis': {
                    'title': {
                        'text': 'Temperatuur in graden Celsius'}}})
        #Step 3: Send the chart object to the template.
    return render(request, 'Sis/plotTemperature.html', {'temperaturechart': cht})

def plotAirPressure(request):
    if(request.method == 'POST'):
        if(request.POST.get('Submit')):
            date = str(request.POST.get('date'))
            source = AirPressure.objects.filter(Detected_on__contains=date)
            #Step 1: Create a DataPool with the data we want to retrieve.
            airpressuredata = \
                DataPool(
                    series=
                    [{'options': {
                        'source': source},
                        'terms': [
                            'Detected_on',
                            'Pressure']}
                     ])

            #Step 2: Create the Chart object
            cht = Chart(
                    datasource = airpressuredata,
                    series_options =
                      [{'options':{
                          'type': 'line',
                          'stacking': False},
                        'terms':{
                          'Detected_on': [
                            'Pressure']
                          }}],
                    chart_options =
                      {'title': {
                           'text': 'Luchtdruk over een bepaalde periode'},
                       'xAxis': {
                            'title': {
                               'text': 'Tijd'}},
                       'yAxis': {
                           'title': {
                               'text': 'Luchtdruk in hPa'}}})
            #Step 3: Send the chart object to the template.
            return render(request, 'Sis/plotAirPressure.html',{'airpressurechart': cht})
    else:
        source = AirPressure.objects.all()
        #Step 1: Create a DataPool with the data we want to retrieve.
        airpressuredata = \
            DataPool(
                series=
                [{'options': {
                    'source': source},
                    'terms': [
                        'Detected_on',
                        'Pressure']}
                ])

        #Step 2: Create the Chart object
        cht = Chart(
                datasource = airpressuredata,
                series_options =
                    [{'options':{
                        'type': 'line',
                        'stacking': False},
                    'terms':{
                        'Detected_on': [
                          'Pressure']
                        }}],
                chart_options =
                    {'title': {
                        'text': 'Luchtdruk over een bepaalde periode'},
                    'xAxis': {
                        'title': {
                            'text': 'Tijd'}},
                    'yAxis': {
                        'title': {
                            'text': 'Luchtdruk in hPa'}}})
        #Step 3: Send the chart object to the template.
        return render(request, 'Sis/plotAirPressure.html',{'airpressurechart': cht})
        
def plotAirComposition(request):
    if(request.method == 'POST'):
        if(request.POST.get('Submit')):
            date = str(request.POST.get('date'))
            composite = str(request.POST.get('composite'))
            print(date)
            print(composite)
            source = AirComposition.objects.filter(Gas=composite,Detected_on__contains=date)
            #Step 1: Create a DataPool with the data we want to retrieve.
            aircompositiondata = \
                DataPool(
                    series=
                    [{'options': {
                        'source': source},
                        'terms': [
                            'Detected_on',
                            'Appearence'
                            ]},
                      ])
            #Step 2: Create the Chart object
            cht = Chart(
                    datasource = aircompositiondata,
                    series_options =
                      [{'options':{
                          'type': 'line',
                          'stacking': False},
                        'terms':{
                          'Detected_on': [
                            'Appearence']
                          }}],
                chart_options =
                      {'title': {
                           'text': 'Luchtsamenstelling over een bepaalde periode'},
                       'xAxis': {
                            'title': {
                               'text': 'Tijd'}},
                       'yAxis': {
                           'title': {
                               'text': 'Luchtsamenstelling in ppm'}}})
             #Step 3: Send the chart object to the template.
            return render(request, 'Sis/plotAirComposition.html',{'aircompositionchart': cht})
    else:
        source = AirComposition.objects.all()
        #Step 1: Create a DataPool with the data we want to retrieve.
        aircompositiondata = \
            DataPool(
                series=
                [{'options': {
                    'source': source},
                    'terms': [
                        'Detected_on',
                        'Appearence'
                        ]},
                    ])
             #Step 2: Create the Chart object
        cht = Chart(
                datasource = aircompositiondata,
                series_options =
                    [{'options':{
                        'type': 'line',
                        'stacking': False},
                    'terms':{
                        'Detected_on': [
                        'Appearence']
                        }}],
                chart_options =
                    {'title': {
                        'text': 'Luchtsamenstelling over een bepaalde periode'},
                    'xAxis': {
                        'title': {
                            'text': 'Tijd'}},
                    'yAxis': {
                        'title': {
                            'text': 'Luchtsamenstelling in ppm'}}})
        #Step 3: Send the chart object to the template.
        return render(request, 'Sis/plotAirComposition.html',{'aircompositionchart': cht})   

def plotMotionDetection(request):
    #Step 1: Create a DataPool with the data we want to retrieve.
    airpressuredata = \
        DataPool(
            series=
            [{'options': {
                'source': MotionDetection.objects.all()},
                'terms': [
                    'Detected_on']}
             ])

    #Step 2: Create the Chart object
    cht = Chart(
            datasource = airpressuredata,
            series_options =
              [{'options':{
                  'type': 'line',
                  'stacking': False},
                'terms':{
                  'Detected_on': [
                      ]
                  }}],
            chart_options =
              {'title': {
                   'text': 'Bewegingsmeldingen over een bepaalde periode'},
               'xAxis': {
                    'title': {
                       'text': 'Tijd'}},
               'yAxis': {
                   'title': {
                       'text': 'Aantal bewegingsmeldingen'}}})
    #Step 3: Send the chart object to the template.
    return render(request, 'Sis/plotMotionDetection.html',{'motiondetectionchart': cht})
