from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^index/$', views.index, name='index'),
    url(r'^addTemperature/$', views.addTemperature, name='addTemperature'),
    url(r'^addAirComposition/$', views.addAirComposition, name='addAirComposition'),
    url(r'^addAirPressure/$', views.addAirPressure, name='addAirPressure'),
    url(r'^addMotionDetection/$', views.addMotionDetection, name='addMotionDetection'),
    url(r'^addSensor/$', views.addSensor, name='addSensor'),
    url(r'^plotTemperature/$', views.plotTemperature, name='plotTemperature'),
    url(r'^plotAirComposition/$', views.plotAirComposition, name='plotAirComposition'),
    url(r'^plotAirPressure/$', views.plotAirPressure, name='plotAirPressure'),
    url(r'^plotMotionDetection/$', views.plotMotionDetection, name='plotMotionDetection')
]
